My Oracle Retail Notes
======================

Links:

 * http://docs.oracle.com/cd/E12448_01/rms/doclist.html
 * RMS 13.2 IG - http://docs.oracle.com/cd/E12448_01/rms/pdf/132/rms-132-06-ig.pdf


RMS 13.2 Installation
---------------------


Pre-requisites:

Oracle Database Enterprise Edition 11gR2 (11.2.0.1) with the following oneoff patches:

 * 9582272:ORA-600[kkdlReadOnDiskDefVal:error]occurs when ALTER TRIGGER is executed.
 * 9100882:SOA:ORA-600[KGHFRE3]SIGNALLED.
 * 9010222:APPSST11GORA-00600[KKSFBC-REPARSE-INFINITE-LOOP]
 * 9932143:[CTS]3FAILURESINJMS/AQRUNDIDN'TGET EXPECTED MSG BACK AND REDELIVERED FLAG
 * 9130054:MASSIVEORA-2051SIGNALLEDDURINGSOA TEST AGAINST AN 11.2.0.1 DATABASE.
 * 9367425:PROCESSCRASHEDWHENUSING11GR2 JDBC/OCI.
 * 9495959: HANG WHEN TWO THREADS TRY TO CREATE THE ENV HANDLE AT THE SAME.

To verify patch level, set ORACLE_HOME, change directory to $ORACLE_HOME/OPatch, and from the command line run opatch lsinventory to show the applied patches.
Note: Patches 9367425 and 9495959 must be applied together. Components:

 * Oracle Database11g
 * Oracle Partitioning
 * Oracle NetServices
 * Oracle CallInterface(OCI)
 * Oracle Programmer
 * Oracle XMLDevelopmentKit
 * Examples CD (FormerlythecompanionCD) Other components:
 * Perl compiler 5.0 or later
 * X-Windows interface
 * ANSI compliant C-compiler (certified with OS and database version).


